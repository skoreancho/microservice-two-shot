import React, { useEffect, useState } from 'react';
import { Link } from 'react-router-dom';


function HatColumn(props) {
  return (
    <div className="col">
        {props.hats.map(data => {
          return (
            <div key={data.id} className="card mb-3 shadow">
              <img src={data.picture_url} className="card-img-top" />
              <div className="card-body">
                <h5 className="card-title">{data.style_name}</h5>
                <h6 className="card-subtitle mb-2 text-muted">
                  Fabric: {data.fabric}
                </h6>
                <h6 className="card-subtitle mb-2 text-muted">
                  Color: {data.color}
                </h6>
                <h6 className="card-subtitle mb-2 text-muted">
                  Location: {data.location.id}
                </h6>
              </div>
              <div className="card-footer">
                <button onClick={ async () => {
                  const hatUrl = `http://localhost:8090/api/hats/${data.id}/`
                  const fetchConfig = {
                    method: "delete",
                  }
                  const response = await fetch (hatUrl, fetchConfig);
                  if (response.ok) {
                    window.location.reload();
                  }
                }
                }
                className="btn btn-danger">
                  Delete
                </button>
              </div>
            </div>
          );
        })}
    </div>
  );
}

function HatList(props) {
  const [hatColumns, setHatColumns] = useState([[], [], []]);

  const fetchData = async () => {
    const response = await fetch ('http://localhost:8090/api/hats/');
    if (response.ok) {
      const data = await response.json();

      const requests = [];
      for (let hat of data.hats) {
        const detailUrl = `http://localhost:8090/api/hats/${hat.id}/`;
        requests.push(fetch(detailUrl));
      }
      const responses = await Promise.all(requests);

      const columns = [[], [], []];

      let i = 0;
      for (const hatResponse of responses) {
        if (hatResponse.ok) {
          const details = await hatResponse.json();
          columns[i].push(details);
          i = i + 1;
          if (i > 2) {
            i = 0;
          }
        } else {
          console.error(hatResponse);
        }
      }
      setHatColumns(columns);
    }
  }

  useEffect(() => {
    fetchData();
  }, []);

  return (
    <>
      <div className="px-4 py-5 my-5 mt-0 text-center bg-info">
        <img className="bg-white rounded shadow d-block mx-auto mb-4" src="/logo.svg" alt="" width="600" />
        <h1 className="display-5 fw-bold">WARDROBIFY!</h1>
        <div className="col-lg-6 mx-auto">
          <p className="lead mb-4">
            Your list of hats!
          </p>
          <div className="d-grid gap-2 d-sm-flex justify-content-sm-center">
            <Link to="/hats/new" className="btn btn-primary btn-lg px-4 gap-3">Create a hat!</Link>
          </div>
        </div>
      </div>
      <div className="container">
        <div className="row">
          {hatColumns.map((hatList, index) => {
            return (
              <HatColumn key={index} hats={hatList} />
            );
          })}
        </div>
      </div>
    </>
  );
}

export default HatList;

import React, { useEffect, useState } from 'react';

function HatForm() {
    const[styleName, setStyleName] = useState('');
    const[fabric, setFabric] = useState('');
    const[color, setColor] = useState('');
    const[pictureUrl, setPictureUrl] = useState('');
    const[location, setLocation] = useState('');
    const[locations, setLocations] = useState([]);

    const fecthData = async () => {
        const url = 'http://localhost:8100/api/locations/';
        const response = await fetch(url);

        if (response.ok) {
            const data = await response.json();
            setLocations(data.locations);
        }
    }

    const handleSubmit = async (event) => {
        event.preventDefault();
        const data = {};
        data.style_name = styleName;
        data.fabric = fabric;
        data.color = color;
        data.picture_url = pictureUrl;
        data.location = location;

        console.log(data);

        const locationUrl = 'http://localhost:8090/api/hats/';
        const fetchConfig = {
            method: "post",
            body: JSON.stringify(data),
            headers: {
                'Content-Type': 'application/json',
            },
        };

        const locationResponse = await fetch(locationUrl, fetchConfig);
        if (locationResponse.ok) {
            const newLocation = await locationResponse.json();
            console.log(newLocation);

            setStyleName('');
            setFabric('');
            setColor('');
            setPictureUrl('');
            setLocation('');
        }
    }

    useEffect(() => {
        fecthData();
    }, []);

    return (
        <div className="row">
        <div className="offset-3 col-6">
          <div className="shadow p-4 mt-4">
            <h1>Create a new hat</h1>
            <form onSubmit={handleSubmit} id="create-location-form">
              <div className="form-floating mb-3">
                <input onChange={(e) => setStyleName(e.target.value)} value={styleName} placeholder="Style Name" required type="text" name="style_name" id="style_name" className="form-control"/>
                <label htmlFor="style_name">Style Name</label>
              </div>
              <div className="form-floating mb-3">
                <input onChange={(e) => setFabric(e.target.value)} value={fabric} placeholder="Fabric" required type="text" name="fabric" id="fabric" className="form-control"/>
                <label htmlFor="fabric">Fabric</label>
              </div>
              <div className="form-floating mb-3">
                <input onChange={(e) => setColor(e.target.value)} value={color} placeholder="Color" required type="text" name="color" id="color" className="form-control"/>
                <label htmlFor="color">Color</label>
              </div>
              <div className="form-floating mb-3">
                <input onChange={(e) => setPictureUrl(e.target.value)} value={pictureUrl} placeholder="Picture URL" required type="text" name="picture_url" id="picture_url" className="form-control"/>
                <label htmlFor="picture_url">Picture URL</label>
              </div>
              <div className="mb-3">
                <select onChange={(e) => setLocation(e.target.value)} value={location} required id="location" name="location" className="form-select">
                  <option value="">Choose a location</option>
                  {locations && locations.map(location => {
                    return (
                        <option key={location.href} value={location.href}>
                            {location.closet_name}
                        </option>
                    );
                  })}
                </select>
              </div>
              <button className="btn btn-primary">Create</button>
            </form>
          </div>
        </div>
      </div>
    );
}

export default HatForm;

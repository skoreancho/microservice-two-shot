import React from 'react';
import { Link } from 'react-router-dom';
import { useState, useEffect} from 'react';


function ShoeColumn(props) {
    return (
      <div className="col">
        {props.shoes.map(data => {
          return (
            <div key={data.id} className="card mb-3 shadow">
              <img src={data.picture_url} className="card-img-top" />
              <div className="card-body">
                <h5 className="card-title">{data.model_name}</h5>
                <h6 className="card-subtitle mb-2 text-muted">
                  {"Bin"} {data.bin.id}
                </h6>
              </div>
              <div className="card-footer">
                <button onClick={async (event) => {
                    const url = `http://localhost:8080/api/shoes/${data.id}`
                    const fetchConfig = {
                        method: "delete"
                    }

                    const response = await fetch(url, fetchConfig)
                    if (response.ok) {
                        window.location.reload()
                    }
                }} className="btn btn-danger">Delete</button>
              </div>
            </div>
          );
        })}
      </div>
    );
}

const ShoesList = (props) => {
    const [shoeColumns, setShoeColumns] = useState([[], [], []]);

    const fetchData = async () => {
        const url = 'http://localhost:8080/api/shoes';
        try {
            const response = await fetch(url);
            if (response.ok) {
                const data = await response.json();

                const requests = [];
                for (let shoe of data.shoes) {
                    const detailUrl = `http://localhost:8080/api/shoes/${shoe.id}`
                    requests.push(fetch(detailUrl))
                }

                const responses = await Promise.all(requests);

                const columns = [[], [], []];

                let i = 0;
                for (const shoeResponse of responses) {
                    if (shoeResponse.ok) {
                        const details = await shoeResponse.json();
                        columns[i].push(details);
                        i = i + 1;
                        if (i > 2) {
                            i = 0;
                        }
                    } else {
                        console.error(shoeResponse)
                    }
                }

                setShoeColumns(columns);
            }
        } catch (e) {
            console.error(e);
        }
    }

    useEffect(() => {
        fetchData();
    }, []);

    const closetUrl = "https://reviewed-com-res.cloudinary.com/image/fetch/s--Wxf3Wrx1--/b_white,c_limit,cs_srgb,f_auto,fl_progressive.strip_profile,g_center,q_auto,w_972/https://reviewed-production.s3.amazonaws.com/1653045979024/8D65F082-4CBB-4C57-9153-83D0C249469A_1_201_a.jpeg"
    return (
        <>
          <div className="px-4 py-5 my-5 mt-0 text-center bg-info">
            <img className="bg-white rounded shadow d-block mx-auto mb-4" src={closetUrl} alt="" width="600" />
            <h1 className="display-5 fw-bold">Shoe Tracker!</h1>
            <div className="col-lg-6 mx-auto">
              <p className="lead mb-4">
                Your virtual assistant for sorting your shoe inventory!
              </p>
              <div className="d-grid gap-2 d-sm-flex justify-content-sm-center">
                <Link to="/shoes/new/" className="btn btn-primary btn-lg px-4 gap-3">Create a Shoe</Link>
              </div>
            </div>
          </div>
          <div className="container">
            <p></p>
            <h2>Shoes</h2>
            <div className="row">
              {shoeColumns.map( (shoeList, index) => {
                return (
                  <ShoeColumn key={index} shoes={shoeList} />
                );
              })}
            </div>
          </div>
        </>
    );
}




export default ShoesList;

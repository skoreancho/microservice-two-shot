import { BrowserRouter, Routes, Route } from 'react-router-dom';
import MainPage from './MainPage';
import Nav from './Nav';
import ShoesList from './ShoesList';
import ShoeForm from './ShoeForm';
import HatForm from './HatForm'
import HatList from './HatList'

function App(props) {
  return (
    <BrowserRouter>
      <Nav />
      <>
        <Routes>
          <Route path="/" element={<MainPage />} />
          <Route path="hats">
            <Route path="new" element={<HatForm />} />
            <Route path="" element={<HatList hats={props.hats} />} />
          </Route>
          <Route path="shoes">
            <Route path="" element={<ShoesList shoes={props.shoes} />} />
            <Route path="new" element={<ShoeForm />} />
          </Route>
        </Routes>
      </>
    </BrowserRouter>
  );
}

export default App;

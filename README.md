# Wardrobify

Team:

* Sean Cho - Shoes
* Edward Sohn - Hats

## Design

## Shoes microservice

For the shoes microservice, two models were created: Shoe and BinVO. The Shoe model was to
outline the type of data that was to be assigned to each shoe instance while the BinVO model was to do the same for BinVO instances. In the case of BinVO instances, they were created through polling in the poller.py file. The poller.py was set to poll data from the wardrobe microservice at the provided url, which in turn created new instances of BinVO objects with the polled data.

## Hats microservice

Began by creating models for Hat and LocationVO.
Created functions to handle GET POST and DELETE request.
Edited the poller to poll Location model data from the Wardrobe API to create a LocationVO instance for each Location instance.
Created React components to handle the front-end.
